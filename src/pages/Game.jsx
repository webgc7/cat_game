import React from "react";
import Board from "../components/Board";
import CalculateWinner from "../components/CalculateWinner";

export default class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null),
        },
      ],

      stepNumber: 0,
      xIsNext: true,
      viewPlayer: "",

      score: { player1: 0, player2: 0, playerIA: 0, tie: 0 },
      scorePlayer1: 0,
      scorePlayer2: 0,
      scorePlayerIA: 0,
      scoreTie: 0,
    };

    this.changeViewPlayer = this.changeViewPlayer.bind(this);

    this.changeScorePlayer1 = this.changeScorePlayer1.bind(this);
    this.changeScorePlayer2 = this.changeScorePlayer2.bind(this);
    this.changeScorePlayerIA = this.changeScorePlayerIA.bind(this);
    this.changeScoreTie = this.changeScoreTie.bind(this);
  }

  changeViewPlayer(string) {
    this.setState({
      viewPlayer: string,
    });
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    if (CalculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([
        {
          squares: squares,
        },
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0,
    });
  }

  changeScorePlayer1() {
    this.setState({ scorePlayer1: this.state.scorePlayer1 + 1 });
  }

  changeScorePlayer2() {
    this.setState({ scorePlayer2: this.state.scorePlayer2 + 1 });
  }

  changeScorePlayerIA() {
    this.setState({ scorePlayerIA: this.state.scorePlayerIA + 1 });
  }

  changeScoreTie() {
    this.setState({ scoreTie: this.state.scoreTie + 1 });
  }

  render() {
    let history = this.state.history;
    let current = history[this.state.stepNumber];
    let winner = CalculateWinner(current.squares);
    let moves = history.map((step, move) => {
      const desc = move ? "Go to move #" + move : "Go to game start";
      return (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)} className="btn btn-primary  btn-sm m-1 w-100">
            {desc}
          </button>
        </li>
      );
    });

    let status;

    if (winner) {
      status = "WINNER: " + winner;
      setTimeout(() => {
        if (winner === "X") {
          this.changeScorePlayer1();
        } else if (winner === "O") {
          this.changeScorePlayer2();
        }
        this.jumpTo(0);
      }, 2500);
    } else {
      if (this.state.stepNumber === 9) {
        status = "Tie";
        setTimeout(() => {
          this.jumpTo(0);
          this.changeScoreTie();
        }, 2500);
      } else {
        status = "Next Player: " + (this.state.xIsNext ? "X" : "O");
      }
    }

    return (
      <div className="game">
        <div className="game-board">
          <h2>{status}</h2>
          <br />
          <Board squares={current.squares} onClick={(i) => this.handleClick(i)} />
          <br />
          <p>Player 1 (X) : {this.state.scorePlayer1}</p>
          <p>Tie: {this.state.scoreTie}</p>
          <p>Player 2 (O) : {this.state.scorePlayer2}</p>
        </div>
        <div className="d-flex align-self-center flex-wrap justify-content-center">
          <div className="game-info">
            <div className="text-center">{status}</div>
            <br />
            <ol className="list-group">{moves}</ol>
            <br />
          </div>
        </div>
      </div>
    );
  }
}
